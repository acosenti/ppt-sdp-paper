# PPT-indistinguishable states via SDP

Python script that implements the linear program used in the papers:

[Cos12] A. Cosentino, PPT-indistinguishable states via semidefinite programming. [arXiv:1205.1031][1]

[CR13] A. Cosentino and V. Russo, Small sets of locally indistinguishable orthogonal maximally entangled states. [arXiv:1307.3232][2]


See Theorem 5 in [Cos12] to understand why the SDP can be written as a linear program.


Tested with Python 2.7.3.

Requires:

- numpy -- [www.numpy.org](http://www.numpy.org/)
- cvxopt -- [www.cvxopt.org](http://www.cvxopt.org/)
 

[1]: http://arxiv.org/abs/1205.1031
[2]: http://arxiv.org/abs/1307.3232

## Usage

In `ppt_indist.py` modify the parameter _k_ and _d_ and run it with the following command:

`python ppt_indist.py`
