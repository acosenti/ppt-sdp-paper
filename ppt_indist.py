"""
  PPT_INDSIT    Primary python script.

   requires: cvxopt, numpy
   authors: Alessandro Cosentino (acosenti@cs.uwaterloo.ca), 
            Vincent Russo        (vrusso@cs.uwaterloo.ca)
   version: 1.00
   last updated: July 6, 2013
"""

from cvxopt import *
import numpy as np
import sys

'''
Constructs a set of indistinguishable states of a given dimension  

@param   t -- the log2 of the dimension of each subsystem
@return  a list of indist. states in dimension 2^t \otimes 2^t
'''
def pptIndistStates(t):

    if t == 2:
        states = [
            [0, 0],
            [1, 3],
            [2, 3],
            [3, 3]
        ]

    if t >= 3:
        states_small = pptIndistStates(t-1)
        states_small_size = len(states_small)
        states = []

        for i in range(0, states_small_size):
            state0 = list(states_small[i])
            state0.extend([0])
            states.append(state0)
            state1 = list(states_small[i])
            state1.extend([1])
            states.append(state1)

    return states

'''
Given a set of states, it returns the value of the lp

@param   states -- a list of tuple, where each tuple specify a states
@return  the optimal value of the lp
'''
def pptLPDual(states):

    t = len(states[1])

    dim = pow(2, t)
    size = pow(dim, 2)
    
    k = len(states)
    
    # hardcoding partial transpose map
    # on bell basis
    pt_map = 0.5*np.matrix([[ 1, 1,-1, 1],
                            [ 1, 1, 1,-1],
                            [-1, 1, 1, 1],
                            [ 1,-1, 1, 1]])
                           
    full_pt_map = np.matrix([1])
    
    for i in range(0,t):
        full_pt_map = np.kron(full_pt_map, pt_map)
        
    # build b
    b = np.array([])
    
    for state in states:
        x = np.array([1])
        for i in range(0, t):
            psi = np.zeros(4)
            psi[state[i]] = 1
            x = np.kron(x, psi)
        b = np.append(b, x)
    
    # build A
    A = Ap = np.eye(size)
    
    
    for i in range(0, k-1):
        A = np.vstack((A, Ap))
        
    Adp = -np.kron(np.eye(k), full_pt_map)
    A = np.hstack((A, Adp))

    # adds the contraints Q_j >= 0
    A_down = np.hstack((np.zeros((size*k,size)), 
                        np.eye(size*k)))
                        
    A = np.vstack((A, A_down))
    b = np.hstack((b, np.zeros(size*k)))

    # build c
    c = np.ones(size)
    c = np.hstack((c, np.zeros(size*k)))
    # run lp
    sol = solvers.lp(-matrix(c), matrix(A), -matrix(b))
    
    sol_obj = sol['primal objective']/k
    
    return sol_obj
   

'''
 --- Main ---
'''

#
# WARNING: 
#    For dimensions above 16, the LP solver may hang 
#    due to the large problem size. 
#

if (len(sys.argv) < 3):
    d = 4
    k = 4
else:
    d = int(sys.argv[1]);
    k = int(sys.argv[2]);
    
t = np.log2(d)

if d < 4:
    print 'Error, dimension of subsystem must be greater than or equal to 4.'
elif np.floor(t) != t:
    print 'Error, dimension of subsystem must be a power of 2.'
elif k > d:
    print 'k must be less than or equal to d'    
else:    
    states = pptIndistStates(t)
    # print states[0:k]
    print pptLPDual(states[0:k])
